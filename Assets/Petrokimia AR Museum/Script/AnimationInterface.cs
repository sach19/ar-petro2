using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationInterface : MonoBehaviour
{
    public Animation animation;

    public void Play()
    {
        animation.Play();
    }
}
