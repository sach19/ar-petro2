using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

[System.Serializable]
public class AudioClipProperties
{
    public AudioClip audioClip;
    public float playAtSeconds;
}

public class ARVideo : MonoBehaviour
{
    public Animator maskotAnimator;
    public Animator videoAnimator;
    public AudioClipProperties audioClipProperties;
    public AudioSource audioSource;
    public Canvas canvas;
    public VideoClip videoClip;
    public VideoPlayer videoPlayer;

    public GameObject maskotIdle;
    public GameObject maskotMove;
    public GameObject playIcon;

    public Action onClick;

    bool isPlaying = false;
    bool audioHasPlayed = false;

    GameObject lastActiveObject;

    public void OnDisable()
    {
        StopVideo(videoPlayer);
        videoPlayer.targetTexture.Release();
        
        maskotIdle.SetActive(true);
        maskotMove.SetActive(false);
    }

    private void Start()
    {
        videoPlayer.targetTexture.Release();

        if(canvas.renderMode == RenderMode.WorldSpace) canvas.worldCamera = Camera.main;
        if(videoClip!= null) videoPlayer.clip = videoClip;

        ShowIdle();

        videoPlayer.Prepare();
        videoPlayer.loopPointReached += StopVideo;

        isPlaying = false;

        onClick += PlayVideo;

        videoAnimator.speed = 0;
    }

    public void PauseVideo()
    {
        maskotAnimator.speed = 0;
        videoAnimator.speed = 0;

        videoPlayer.Pause();
        audioSource.Pause();

        playIcon.SetActive(true);

        isPlaying = false;

        onClick = PlayVideo;

        if(maskotMove.activeInHierarchy)
        {
            lastActiveObject = maskotMove;
        }
        else
        {
            lastActiveObject = maskotIdle;
        }
    }

    public void PlayVideo()
    {
        if(!audioHasPlayed)
        {
            if (lastActiveObject == null)
            {
                ShowMove();
            }
            else
            {
                lastActiveObject.SetActive(true);
            }
        }

        playIcon.SetActive(false);

        videoAnimator.speed = 1;
        maskotAnimator.speed = 1;
        // audioSource.Play();
        videoPlayer.Play();

        isPlaying = true;

        onClick = PauseVideo;
    }

    public void StopVideo(VideoPlayer _source)
    {
        maskotIdle.SetActive(true);
        maskotMove.SetActive(false);

        lastActiveObject = null;
        playIcon.SetActive(true);

        _source.Stop();
        audioSource.Stop();

        audioHasPlayed = false;

        isPlaying = false;

        onClick = PlayVideo;

        videoAnimator.speed = 0;
    }

    public void OnClick()
    {
        onClick?.Invoke();
    }

    public void ShowIdle()
    {
        maskotIdle.SetActive(true);
        maskotMove.SetActive(false);
    }

    public void ShowMove()
    {
        maskotIdle.SetActive(false);
        maskotMove.SetActive(true);
    }

    public void Update()
    {
        // Debug.Log($"{videoPlayer.time}, {audioSource.clip.length}, {audioSource.isPlaying}, {isPlaying}, {audioHasPlayed}");

        if(audioSource.time >= audioSource.clip.length)// && !audioHasPlayed)
        {
            ShowIdle();

            audioHasPlayed = true;
        }
        
        if(videoPlayer.time > 1f && !audioSource.isPlaying && isPlaying && !audioHasPlayed)
        {
            audioSource.Play();
        }
    }
}
