using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomScrollViewBehaviour : MonoBehaviour
{
    public int itemCount;
    public float scrollWidth;
    public float pos;
    public float step;

    public CanvasGroup scrollbarCanvasGroup;
    public RectTransform contentRectTransform;
    public Scrollbar scrollbar;
    public ScrollRect scrollRect;

    RectTransform previouslySelected, recentlySelected;

    public void SelectItem(int _index)
    {
        recentlySelected = scrollRect.content.GetChild(_index).GetComponent<RectTransform>();
        ScrollTo(_index);
    }
    
    void ScrollTo(int _index)
    {
        float mid = (itemCount+1f) / 2;
        int sign = _index + 1 <= mid ? -1 : 1;
        bool isEven = itemCount % 2 == 0;

        Vector2 newPos = Vector2.zero;
        newPos.x = isEven ? step * ((int)(_index + 1 - mid)) + (pos * sign) : (_index + 1 - mid) * step;

        float target = Mathf.Clamp((newPos.x + scrollWidth) / (scrollWidth * 2f), 0f, 1f);

        ScrollbarTweening(target);
    }

    void ScrollbarTweening(float _target)
    {
        Vector2 newRecentAnchoredPos;
        Vector2 newPreviousAnchoredPos;

        bool isSame = previouslySelected == recentlySelected;
        Debug.Log(isSame);

        scrollbarCanvasGroup.interactable = false;

        newRecentAnchoredPos = recentlySelected.anchoredPosition;
        if(previouslySelected!=null)
        {
            newPreviousAnchoredPos = recentlySelected.anchoredPosition;
        }

        Sequence s = DOTween.Sequence();

        s.Append(DOTween.To(x => scrollRect.horizontalScrollbar.value = x, scrollRect.horizontalScrollbar.value, _target, 0.5f).OnComplete(() =>
        {
            scrollbarCanvasGroup.interactable = true;
            previouslySelected = recentlySelected;
        }));
        s.Join(recentlySelected.DOAnchorPosY(18f, .5f));
        if(previouslySelected!=null && !isSame)
        {
            s.Join(previouslySelected.DOAnchorPosY(0f, .5f));
        }
    }
}
