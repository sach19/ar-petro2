using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MilestoneScript : MonoBehaviour
{
    public CanvasGroup canvasGroup;
    public GameObject messagePanel;
    public RectTransform movablePanel;
    public RectTransform imageMasking;
    public RectTransform moveMasking;
    public TMPro.TextMeshProUGUI messageTitle;
    public TMPro.TextMeshProUGUI messageInformation;
    public GameObject KeboIdle;
    public GameObject KeboSpeak;
    public GameObject KeboWalk;
    int currentInfo = 0;
    int targetInfo = 0;
    [SerializeField]
    float space;
    public float speed;

    [System.Serializable]
    public struct Info
    {
        public string title;
        public string information;
        public AudioClip[] dub;
    }
    public List<Info> Infos;

    float basePosMovablePanel;
    float basePosImagePanel;
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        basePosMovablePanel = movablePanel.anchoredPosition.x;
        basePosImagePanel = imageMasking.anchoredPosition.x;
        currentInfo = 0;
        messagePanel.SetActive(false);
        audioSource = GetComponent<AudioSource>();
        audioSource.Stop();

        messageTitle.text = Infos[0].title;
        messageInformation.text = Infos[0].information;
        messagePanel.SetActive(true);
        StartCoroutine(AudioPlaySequence());
    }
    public void MovingYear(int index){
        // print("move");
        targetInfo = index;
        messageTitle.text = Infos[index].title;
        messageInformation.text = Infos[index].information;
        messagePanel.SetActive(false);
        StopAllCoroutines();
        StartCoroutine(Move());
    }
    float currentSpeed;
    void Walk(){
        KeboWalk.SetActive(true);
        KeboIdle.SetActive(false);
        KeboSpeak.SetActive(false);
        if(currentInfo > targetInfo){
            currentSpeed = speed;
            KeboWalk.transform.localScale = new Vector3(-1,1,1);
        }else{
            currentSpeed = -speed;
            KeboWalk.transform.localScale = new Vector3(1,1,1);
        }
    }
    void Idle(){
        KeboWalk.SetActive(false);
        KeboSpeak.SetActive(false);
        KeboIdle.SetActive(true);
    }
    void Speak(){
        KeboWalk.SetActive(false);
        KeboSpeak.SetActive(true);
        KeboIdle.SetActive(false);
    }
    IEnumerator Move(){
        Walk();
        audioSource.Stop();

        canvasGroup.interactable = false;

        float currentPos = movablePanel.anchoredPosition.x;
        float targetPos = basePosMovablePanel-(targetInfo*space);
        float threshold = 4;
        while (Mathf.Abs(targetPos-currentPos) > threshold)
        {
            currentPos += currentSpeed;
            print(targetPos+"/"+currentPos+"/"+Mathf.Abs(targetPos-currentPos));
            movablePanel.anchoredPosition = new Vector2(movablePanel.anchoredPosition.x + currentSpeed,movablePanel.anchoredPosition.y);

            imageMasking.anchoredPosition = new Vector2(imageMasking.anchoredPosition.x - currentSpeed,imageMasking.anchoredPosition.y);
            moveMasking.anchoredPosition = new Vector2(moveMasking.anchoredPosition.x + currentSpeed,moveMasking.anchoredPosition.y);
            yield return null;
        }
        currentInfo = targetInfo;
        // Idle();
        canvasGroup.interactable = true;
        messagePanel.SetActive(true);
        StartCoroutine(AudioPlaySequence());
    }
    IEnumerator AudioPlaySequence(){
        for (var i = 0; i < Infos[targetInfo].dub.Length; i++)
        {
            audioSource.clip = Infos[targetInfo].dub[i];
            audioSource.Play();
            Speak();
            while (audioSource.isPlaying)
            {
                yield return null;
            }
            audioSource.Stop();

            Idle();
            // delay for next song
            yield return new WaitForSeconds(1f);
        }
    }
}
