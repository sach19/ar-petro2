using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RTPostRender : MonoBehaviour
{
    public Camera target;

    public void Start()
    {
        Release();
    }

    // Start is called before the first frame update
    public void Update()
    {
        Release();
    }

    public void Release()
    {
        if(target.targetTexture!=null)
        {
            target.targetTexture.Release();
        }
    }
}
