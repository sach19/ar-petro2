using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;
using UnityEngine;

public class SendEmail : MonoBehaviour
{
    public string from;
    public string fromPassword;
    public string to;
    public string subject;
    public string body;

    public string directory;
    public string folderName;

    public Sprite uploadedImage;

    //App-password: jutgcgvipnfeuebd (khalilandaal@gmail.com)

    public void SplitDirectories()
    {
        foreach (var s in directory.Split('/'))
        {
            Debug.Log(s);
        }
    }

    public void CheckFolderName()
    {
        StartCoroutine(GoogleDriveUtility.Instance.GetFileByPathRoutine(folderName, (folderId) =>
        {
            Debug.Log($"{folderName} is found!");
        }, () =>
        {
            Debug.Log($"{folderName} is not found!");
        }));
    }

    public void UploadFile()
    {
        var jpg = ImageConversion.EncodeToJPG(uploadedImage.texture);

        DateTime now = DateTime.Now;

        GoogleDriveUtility.UploadFile(jpg, $"{now.ToString("HHmmss")}.jpg", folderName, null, null);
    }

    public void EmailSend(string _from, string _to, string _subject, string _body, string _appPassword, string _attachmentPath, Action<object, AsyncCompletedEventArgs> onCompleted)
    {
        Debug.Log("Sending email...");

        MailMessage mail = new MailMessage();
        SmtpClient client = new SmtpClient("smtp.gmail.com");
        mail.From = new MailAddress(_from);
        mail.To.Add(_to);
        mail.Subject = _subject;
        mail.Body = _body;

        System.Net.Mail.Attachment attachment;
        attachment = new System.Net.Mail.Attachment(_attachmentPath);
        mail.Attachments.Add(attachment);

        client.Port = 587;
        client.Credentials = new System.Net.NetworkCredential(_from, _appPassword);
        client.EnableSsl = true;
        client.SendCompleted += new SendCompletedEventHandler(onCompleted);

        client.SendAsync(mail, null);

    }
}
