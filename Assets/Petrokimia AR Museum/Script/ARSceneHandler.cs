using DG.Tweening;
using System;
using System.Net;
using System.Net.Mail;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;
using System.Runtime.InteropServices;

public class ARSceneHandler : MonoBehaviour
{
    static ARSceneHandler instance;

    public static ARSceneHandler Active
    {
        get
        {
            instance = FindObjectOfType<ARSceneHandler>(true);
            return instance;
        }
    }

    public ARCameraManager ARCamManager;

    public Button sendButton;

    public Camera _ARCamera;
    public CanvasGroup resultCanvasGroup;

    public GameObject capturingGroup;
    public GameObject backgroundGroup;
    public GameObject scanningGroup;
    public GameObject sendingIndicator;
    public GameObject sendFailedIndicator;
    public GameObject sendSuccessIndicator;

    public SeekButton markerListSeek;

    public TMP_Text countDisplay;
    public TMP_InputField emailName;

    public Animation resultAnimation;

    public Sprite debugPhotoResult;
    public Sprite frame;
    public Sprite frameMask;

    public Image photoDisplay;

    public ComputeShader mixImageShader;

    public Action<Sprite> onScreenshot;

    public UnityEvent onTakingPhoto;
    public UnityEvent onShowingPhoto;
    public UnityEvent onNewObjectShow;

    public void OnObjectAppear()
    {
        scanningGroup.SetActive(false);
    }
    public void BackToMain()
    {
        // LoaderUtility.Deinitialize();
        SceneManager.LoadScene(0);
    }

    public void CountingTween()
    {
        ARObjectHandler.Active.debugCanvas.SetActive(false);
        sendFailedIndicator.SetActive(false);
        sendSuccessIndicator.SetActive(false);
        backgroundGroup.SetActive(false);
        onTakingPhoto?.Invoke();
        capturingGroup.SetActive(true);

        Sequence sequence = DOTween.Sequence();

        sequence.Append(countDisplay.DOFade(1f, 0f).OnComplete(() =>
        {
            countDisplay.text = "3";
        }));
        sequence.Append(countDisplay.DOFade(0f, 1f).OnComplete(() =>
        {
            countDisplay.text = "2";
            countDisplay.color = Color.white;
        }));
        sequence.Append(countDisplay.DOFade(0f, 1f).OnComplete(() =>
        {
            countDisplay.text = "1";
            countDisplay.color = Color.white;
        }));
        //sequence.Append(countDisplay.DOFade(0f, 1f)).OnComplete(() =>
        //{
        //    countingGroup.transform.GetChild(0).gameObject.SetActive(false);
        //});
        sequence.Append(countDisplay.DOFade(0f, 1f)).OnComplete(() =>
        {
            StartCoroutine(TakePhoto());
        });
    }

    public void Reset()
    {
        scanningGroup.SetActive(false);
        ARObjectHandler.Active.Reset();
    }

    public void ShowScan()
    {
        scanningGroup.SetActive(true);
    }

    IEnumerator TakePhoto()
    {
        capturingGroup.SetActive(false);

        yield return new WaitForEndOfFrame();

        Debug.Log($"{Screen.width}, {Screen.height}");

        int width = Screen.width;
        int height = Screen.height;

        // _ARCamera.targetTexture = RenderTexture.GetTemporary(width, height, 16);
        // RenderTexture rt = _ARCamera.targetTexture;

        // yield return new WaitForEndOfFrame();

        Rect rect = new Rect(0, 0, width, height);
        Texture2D photoResult = new Texture2D(width, height, TextureFormat.ARGB32, false);
        // photoResult.ReadPixels(rect, 0, 0);
        // photoResult.LoadRawTextureData(photoResult.GetRawTextureData());
        // photoResult.Apply();

        photoResult = ScreenCapture.CaptureScreenshotAsTexture(ScreenCapture.StereoScreenCaptureMode.BothEyes);

        debugPhotoResult = ProcessImage(rect, frame.texture, photoResult);
        onScreenshot?.Invoke(Sprite.Create(photoResult, rect, Vector2.one * .5f));

        _ARCamera.targetTexture = null;

        yield return ShowResult(debugPhotoResult);

        yield return null;
    }

    IEnumerator ShowResult(Sprite _result)
    {
        photoDisplay.sprite = _result;

        resultAnimation.Play();

        backgroundGroup.SetActive(true);

        ARObjectHandler.Active.debugCanvas.SetActive(true);

        onShowingPhoto?.Invoke();

        yield break;
    }

    Sprite ProcessImage(Rect _rect, Texture2D _frame, Texture2D _photo)
    {
        /*if (_frame.width != _photo.width || _frame.height != _photo.height)
        {
            Debug.Log("Lolak lolok super");
            return null;
        }*/

        RenderTexture result = new RenderTexture(_frame.width, _frame.height, 16);
        result.enableRandomWrite = true;

        RenderTexture frame = new RenderTexture(_frame.width, _frame.height, 16);
        frame.enableRandomWrite = true;
        Graphics.Blit(_frame, frame);

        RenderTexture photo = new RenderTexture(_frame.width, _frame.height, 16);
        photo.enableRandomWrite = true;
        Graphics.Blit(_photo, photo);

        RenderTexture frameMasking = new RenderTexture(_frame.width, _frame.height, 16, RenderTextureFormat.ARGB32);
        frameMasking.enableRandomWrite = true;
        Graphics.Blit(frameMask.texture, frameMasking);

        Vector3Int threadGroups = new Vector3Int(((int)_rect.width / 8) + 1, ((int)_rect.height / 8), 1);

        int kernelID = mixImageShader.FindKernel("CSMain");

        mixImageShader.SetTexture(kernelID, "Result", result);
        mixImageShader.SetTexture(kernelID, "_inputFrame", frame);
        mixImageShader.SetTexture(kernelID, "_inputMask", frameMasking);
        mixImageShader.SetTexture(kernelID, "_inputPhoto", photo);
        mixImageShader.Dispatch(kernelID, threadGroups.x, threadGroups.y, threadGroups.z);

        return Sprite.Create(toTexture2D(result, _rect), _rect, Vector2.one * .5f);
    }

    Texture2D toTexture2D(RenderTexture rTex, Rect _rect)
    {
        Texture2D tex = new Texture2D((int)_rect.width, (int)_rect.height, TextureFormat.ARGB32, false);
        // ReadPixels looks at the active RenderTexture.
        RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        tex.Apply();
        return tex;
    }

    public static void InvokeTakePhoto()
    {
        Debug.Log("Taking photo!");
    }

    /*[DllImport("__Internal")]
    private static extern void SaveToPhotosAlbum(string filepath);
    public void SaveToPhotos(string imagePath)
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            SaveToPhotosAlbum(imagePath);
        }
    }*/

    public void SendPhoto()
    {
        /*if(emailName.text.Contains("@gmail.com"))
        {*/
            var jpg = ImageConversion.EncodeToJPG(photoDisplay.sprite.texture);
            string path = $"{Application.persistentDataPath}/result.PNG";

            Debug.Log($"{emailName.text}, {Application.persistentDataPath}");

            File.WriteAllBytes(path, jpg);

            sendingIndicator.SetActive(true);
            sendFailedIndicator.SetActive(false);
            sendSuccessIndicator.SetActive(false);
            resultCanvasGroup.interactable = false;

            DateTime now = DateTime.Now;

            Debug.Log($"{now.ToString("HHmmss")}.jpg, {now.ToString("dd MMMM yyyy")}");

#if UNITY_IOS
        //SaveToPhotosAlbum(path);
#endif

        sendingIndicator.SetActive(false);

        //GoogleDriveUtility.UploadFile(jpg, $"{now.ToString("HHmmss")}.jpg", $"NPK Interactive Learning Gallery/{now.ToString("dd MMMM yyyy")}", () =>
        //{
        //    //Berhasil
        //    sendFailedIndicator.SetActive(false);
        //    sendSuccessIndicator.SetActive(true);

        //    sendingIndicator.SetActive(false);
        //    resultCanvasGroup.interactable = true;
        //}, ()=>
        //{
        //    //Ulang
        //    sendFailedIndicator.SetActive(true);
        //    sendSuccessIndicator.SetActive(false);

        //    sendingIndicator.SetActive(false);
        //    resultCanvasGroup.interactable = true;
        //});

        /*SendEmail.EmailSend("petromuseumnpk@gmail.com", emailName.text, "Hasil Foto Petro NPK Museum", "Terima kasih telah mengunjungi Petro NPK Museum", "ulsxxgtnmebkexal", path, (o, args)=>
        {
            MailMessage msg = (MailMessage)args.UserState;

            if(args.Cancelled || args.Error != null)
            {
                //Ulang
                sendFailedIndicator.SetActive(true);
                sendSuccessIndicator.SetActive(false);
            }
            else
            {
                //Berhasil
                sendFailedIndicator.SetActive(false);
                sendSuccessIndicator.SetActive(true);
            }

            sendingIndicator.SetActive(false);
            resultCanvasGroup.interactable = true;
            // sendButton.interactable = true;

            if(msg != null)
            {
                msg.Dispose();
            }
        });*/
        /*}*/
    }
}
