using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;
using UnityGoogleDrive;

public static partial class Extensions
{
    /// <summary>
    ///     A string extension method that get the string after the specified string.
    /// </summary>
    /// <param name="this">The @this to act on.</param>
    /// <param name="value">The value to search.</param>
    /// <returns>The string after the specified value.</returns>
    public static string GetAfter(this string @this, string value)
    {
        if (@this.IndexOf(value) == -1)
        {
            return "";
        }
        return @this.Substring(@this.LastIndexOf(value) + value.Length);
    }

    /// <summary>
    ///     A string extension method that get the string before the specified string.
    /// </summary>
    /// <param name="this">The @this to act on.</param>
    /// <param name="value">The value to search.</param>
    /// <returns>The string before the specified value.</returns>
    public static string GetBefore(this string @this, string value)
    {
        if (@this.IndexOf(value) == -1)
        {
            return "";
        }
        return @this.Substring(0, @this.LastIndexOf(value));
    }
}

public class GoogleDriveUtility : MonoBehaviour
{
    public static GoogleDriveUtility Instance
    {
        get
        {
            return FindObjectOfType<GoogleDriveUtility>(true);
        }
    }

    public bool debugUploadToParentFolder;
    public string UploadFilePath;
    public string targetFolderID;
    public string newDriveFolderName;

    public string folderName;

    string result;
    GoogleDriveFiles.CreateRequest createRequest;
    GoogleDriveFiles.ListRequest listRequest;

    public static void UploadFile(byte[] _data, string _uploadedFileName, string _targetFolder, Action onSuccess, Action onError)
    { 
        Debug.Log($"Sending: {_uploadedFileName}\nFolder: {_targetFolder}");

        /*Instance.StartCoroutine(Instance.GetFileByPathRoutine(_targetFolder, () =>
        {
            Instance.UploadFile(_data, _uploadedFileName, onSuccess, onError);
        }, ()=>
        {
            Instance.UploadFolder(_targetFolder, () =>
            {
                Instance.UploadFile(_data, _uploadedFileName, onSuccess, onError);
            }, onError);
        }));*/

        Instance.StartCoroutine(UploadingFile(_data, _uploadedFileName, _targetFolder, onSuccess, onError));
    }

    private static IEnumerator UploadingFile(byte[] _data, string _uploadedFileName, string _targetFolder, Action onSuccess, Action onError)
    {
        yield return CheckFolders(_targetFolder);

        Instance.UploadFile(_data, _uploadedFileName, onSuccess, onError);

        yield break;
    }

    private static IEnumerator CheckFolders(string _targetFolder)
    {
        var subStrings = _targetFolder.Split('/');
        string directory = "";
        string rootID = "root";

        foreach(var s in subStrings)
        {
            Debug.Log($"For folder name: {s}, its root ID is: {rootID}");
            directory += s;

            bool isHalt = true;

            yield return Instance.GetFileByPathRoutine(directory, (folderId)=>
            {
                Debug.Log($"{directory} is found!");
                rootID = folderId;
                isHalt = false;
            },() =>
            {
                Instance.UploadFolder(s, rootID, (newID) =>
                {
                    rootID = newID;
                    isHalt = false;
                }, ()=>
                {
                    Debug.LogError($"Failed creating new folder called {s} in {rootID}");
                    isHalt = false;
                });
            });

            while(isHalt)
            {
                yield return null;
            }

            directory += "/";
        }

        Instance.targetFolderID = rootID;

        yield break;
    }

    private void UploadFile(byte[] _data, string _uploadedFileName, Action onSuccess, Action onError)
    {
        var file = new UnityGoogleDrive.Data.File() { Name = _uploadedFileName, Content = _data };//, Permissions = new List<UnityGoogleDrive.Data.Permission> { new UnityGoogleDrive.Data.Permission() {Type = "anyone" } } };
        if (!string.IsNullOrEmpty(targetFolderID)) file.Parents = new List<string> { targetFolderID };
        
        createRequest = GoogleDriveFiles.Create(file);
        createRequest.Fields = new List<string> { "id", "name", "size", "createdTime" };

        createRequest.Send().OnDone += (f) =>
        {
            PrintResult(f);
            if (createRequest.IsError)
            {
                onError?.Invoke();
            }
            else
            {
                onSuccess?.Invoke();
            }
        };
    }

    private void UploadFolder(string _newFolderName, string _rootID, Action<string> onSuccess, Action onError)
    {
        Debug.Log($"Creating new folder: {_newFolderName}");

        var file = new UnityGoogleDrive.Data.File() { Name = _newFolderName, MimeType = "application/vnd.google-apps.folder" };
        file.Parents = new List<string> { string.IsNullOrEmpty(_rootID) ? "root" : _rootID };

        createRequest = GoogleDriveFiles.Create(file);
        createRequest.Fields = new List<string> { "id" };
        createRequest.Send().OnDone += (f) =>
        {
            targetFolderID = f.Id;

            //PrintResult(f);
            if (createRequest.IsError)
            {
                onError?.Invoke();
            }
            else
            {
                onSuccess?.Invoke(f.Id);
            }
        };
    }

    private void PrintResult(UnityGoogleDrive.Data.File file)
    {
        result = string.Format("Name: {0} Size: {1:0.00}MB Created: {2:dd.MM.yyyy HH:MM:ss}\nID: {3}",
                file.Name,
                file.Size * .000001f,
                file.CreatedTime,
                file.Id);
        Debug.Log(result);
    }

    public IEnumerator GetFileByPathRoutine(string filePath, Action<string> onSuccess, Action onError)
    {
        // A folder in Google Drive is actually a file with the MIME type 'application/vnd.google-apps.folder'. 
        // Hierarchy relationship is implemented via File's 'Parents' property. To get the actual file using it's path 
        // we have to find ID of the file's parent folder, and for this we need IDs of all the folders in the chain. 
        // Thus, we need to traverse the entire hierarchy chain using List requests. 
        // More info about the Google Drive folders: https://developers.google.com/drive/v3/web/folder.

        targetFolderID = "";

        var fileName = filePath.Contains("/") ? filePath.GetAfter("/") : filePath;
        var parentNames = filePath.Contains("/") ? filePath.GetBefore("/").Split('/') : null;

        // Resolving folder IDs one by one to find ID of the file's parent folder.
        var parendId = "root"; // 'root' is alias ID for the root folder in Google Drive.
        if (parentNames != null)
        {
            for (int i = 0; i < parentNames.Length; i++)
            {
                listRequest = new GoogleDriveFiles.ListRequest();
                listRequest.Fields = new List<string> { "files(id)" };
                listRequest.Q = $"'{parendId}' in parents and name = '{parentNames[i]}' and mimeType = 'application/vnd.google-apps.folder' and trashed = false";

                yield return listRequest.Send();

                if (listRequest.IsError || listRequest.ResponseData.Files == null || listRequest.ResponseData.Files.Count == 0)
                {
                    result = $"Failed to retrieve '{parentNames[i]}' part of '{filePath}' file path.";

                    onError?.Invoke();
                    yield break;
                }

                if (listRequest.ResponseData.Files.Count > 1)
                    Debug.LogWarning($"Multiple '{parentNames[i]}' folders been found.");

                parendId = listRequest.ResponseData.Files[0].Id;
            }
        }

        // Searching the file.
        listRequest = new GoogleDriveFiles.ListRequest();
        listRequest.Fields = new List<string> { "files(id, size, modifiedTime)" };
        listRequest.Q = $"'{parendId}' in parents and name = '{fileName}'";

        yield return listRequest.Send();

        if (listRequest.IsError || listRequest.ResponseData.Files == null || listRequest.ResponseData.Files.Count == 0)
        {
            result = $"Failed to retrieve '{filePath}' file.";

            onError?.Invoke();
            yield break;
        }

        if (listRequest.ResponseData.Files.Count > 1)
            Debug.LogWarning($"Multiple '{filePath}' files been found.");

        var file = listRequest.ResponseData.Files[0];

        result = string.Format("ID: {0} Size: {1:0.00}MB Modified: {2:dd.MM.yyyy HH:MM:ss}",
            file.Id, file.Size * .000001f, file.CreatedTime);

        Debug.Log(result);

        targetFolderID = file.Id;

        onSuccess?.Invoke(file.Id);
    }
}
