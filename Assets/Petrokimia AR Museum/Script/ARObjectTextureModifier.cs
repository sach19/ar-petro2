using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARObjectTextureModifier : MonoBehaviour
{
    public List<Sprite> sprites;
    public SpriteRenderer targetRenderer;

    public CustomScrollViewBehaviour tmpCustomScrollView;

    public void SetSprite(int _index)
    {
        targetRenderer.sprite = sprites[_index];

        tmpCustomScrollView.SelectItem(_index);
    }
}
