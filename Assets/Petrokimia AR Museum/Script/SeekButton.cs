using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class SeekButton : MonoBehaviour
{
    bool toggle = true;

    public float hideOffset;
    public float contentHideOffset;
    public float tweeningDuration;

    public CanvasGroup canvasGroup;
    public RectTransform transform;
    public RectTransform content;

    public UnityEvent onHide;
    public UnityEvent onSeek;

    public void Start()
    {
        Toggle(false);
    }

    public void Toggle(bool _state, bool _useTweening)
    {
        toggle = _state;
        Toggle(_useTweening);
    }

    public void OnClick()
    {
        toggle = !toggle;
        Toggle(true);
    }

    private void Toggle(bool _useTweening)
    {
        if(toggle)
        {
            onSeek?.Invoke();
        }
        else
        {
            onHide?.Invoke();
        }

        canvasGroup.interactable = false;

        Sequence sequence = DOTween.Sequence();

        sequence.Append(transform.DOAnchorPosX(toggle ? 0f : hideOffset, _useTweening? tweeningDuration : 0f));
        sequence.Join(content.DOAnchorPosX(toggle ? 0f : contentHideOffset, _useTweening ? tweeningDuration : 0f).OnComplete(() =>
        {
            canvasGroup.interactable = true;
        }));

        /*sequence.Append(countDisplay.DOFade(1f, 0f).OnComplete(() =>
        {
            countDisplay.text = "3";
        }));
        sequence.Append(countDisplay.DOFade(0f, 1f).OnComplete(() =>
        {
            countDisplay.text = "2";
            countDisplay.color = Color.white;
        }));
        sequence.Append(countDisplay.DOFade(0f, 1f).OnComplete(() =>
        {
            countDisplay.text = "1";
            countDisplay.color = Color.white;
        }))*/;
        //sequence.Append(countDisplay.DOFade(0f, 1f)).OnComplete(() =>
        //{
        //    countingGroup.transform.GetChild(0).gameObject.SetActive(false);
        //});
        /*sequence.Append(countDisplay.DOFade(0f, 1f)).OnComplete(() =>
        {
            StartCoroutine(TakePhoto());
        });*/
    }
}
