using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
//using UnityEngine.XR.ARCore;
using UnityEngine.XR.ARFoundation;

public class EventList : MonoBehaviour
{
    public GameObject checkIfActive;
    bool activation = false;
    public UnityEvent triggerOnActive, triggerOnDeactive;
    public List<UnityEvent> events;
    public ARSession arSession;

    private void FixedUpdate()
    {
        if (checkIfActive)
        {
            if (checkIfActive.activeSelf == activation)
                return;
            if (checkIfActive.activeSelf)
                triggerOnActive.Invoke();
            else triggerOnDeactive.Invoke();
            activation = checkIfActive.activeSelf;
        }
    }

    private void OnLevelWasLoaded(int level)
    {
        /*LoaderUtility.Deinitialize();
        LoaderUtility.Initialize();*/
        /*if (arSession)
            arSession.Reset();*/
        /*var xrManagerSettings = UnityEngine.XR.Management.XRGeneralSettings.Instance.Manager;
        xrManagerSettings.DeinitializeLoader();
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex); // reload current scene
        xrManagerSettings.InitializeLoaderSync();*/
    }
}
