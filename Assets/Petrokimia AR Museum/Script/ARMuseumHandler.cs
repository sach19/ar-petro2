using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARFoundation.Samples;
using UnityEngine.XR.ARSubsystems;

[System.Serializable]
public struct ARMarkerLibrary
{
    public XRReferenceImageLibrary imageLibrary;
    public GameObject prefabLibrary;
}

[System.Serializable]
public class ARMuseumHandler : MonoBehaviour
{
    static ARMuseumHandler instance;
    public static ARMuseumHandler Instance
    {
        get
        {
            if(instance==null)
            {
                instance = FindObjectOfType<ARMuseumHandler>(true);
            }
            return instance;
        }
    }

    [Header("AR Component")]
    public ARTrackedImageManager trackedImageManager;
    public PrefabImagePairManager imagePairManager;

    [Header("UI Group")]
    public GameObject mainMenuGroup;

    [SerializeField, NonReorderable, Header("Marker Library")]
    public List<ARMarkerLibrary> markerLibrary;

    private void Start()
    {
        LoaderUtility.Deinitialize();
        DontDestroyOnLoad(Instance);
    }

    public void EnableGroup(int _index)
    {
        LoaderUtility.Initialize();
        SceneManager.LoadScene(_index + 1);
    }

    public void Unload()
    {
        trackedImageManager.referenceLibrary = null;
        imagePairManager.imageLibrary = null;
    }
}
