using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ARObjectHandler : MonoBehaviour
{
    static ARObjectHandler instance;
    public static ARObjectHandler Active
    {
        get
        {
            instance = FindObjectOfType<ARObjectHandler>(true);
            return instance;
        }
    }

    public float movingForce;
    public float maxScale;
    public float minScale;

    public bool isDebug { get; set; }
    public float scaleFactor;

    public GameObject debugCanvas;
    public GameObject takePhotoGroup;
    public Slider scaleSlider;
    public Transform ARObjectTransform;
    public TMP_Text debugScaleDisplay;
    public UnityEvent onStart;

    float unitScale = 1;
    float delta;
    Vector3 scale;

    private void Start()
    {
        onStart?.Invoke();
        
        scale = ARObjectTransform.localScale;
        scaleFactor = 1f;
        delta = maxScale - minScale;

        if (scaleSlider != null)
        {
            Rescale(scaleSlider.value);
        }
    }

    private void OnEnable()
    {
        ARSceneHandler.Active?.markerListSeek.Toggle(false, true);
        ARSceneHandler.Active?.onNewObjectShow?.Invoke();
        ARSceneHandler.Active?.OnObjectAppear();
    }

    public void InvokeTakePhoto()
    {
        takePhotoGroup.SetActive(false);
        ARSceneHandler.Active.CountingTween();
        scaleSlider.gameObject.SetActive(false);
    }

    public void Rescale(float _unitScale)
    {
        if(_unitScale > 0) unitScale = _unitScale;
        // Vector3 newScale = scale * unitScale * scaleFactor;

        float rangedScale = minScale + (_unitScale * delta);
        Vector3 newScale = rangedScale * scale;

        ARObjectTransform.localScale = newScale;

        if(debugScaleDisplay!=null)
        {
            debugScaleDisplay.text = String.Format("{0:0.00}", _unitScale);
        }
    }

    public void ChangeScaleFactor(string _string)
    {
        scaleFactor = float.Parse(_string);
        Rescale(unitScale);
    }

    public void BackToScanView()
    {
        ARSceneHandler.Active?.ShowScan();
    }

    public void Reset()
    {
        scaleSlider.gameObject.SetActive(true);
        takePhotoGroup.SetActive(true);
    }

    public void ResetPosition()
    {
        ARObjectTransform.localPosition = Vector3.zero;
    }

    public void MoveX(bool isPositive)
    {
        Move(new Vector3((isPositive ? 1f : -1f),0,0));
        // Vector3 pos = ARObjectTransform.position;
        // pos.x += (isPositive ? 1f : -1f) * movingForce;

        // ARObjectTransform.position = pos;
    }

    public void MoveY(bool isPositive)
    {
        Move(new Vector3(0,(isPositive ? 1f : -1f),0));
        // Vector3 pos = ARObjectTransform.position;
        // pos.y += (isPositive ? 1f : -1f) * movingForce;

        // ARObjectTransform.position = pos;
    }

    public void MoveZ(bool isPositive)
    {
        Move(new Vector3(0,0,(isPositive ? 1f : -1f)));
        // Vector3 pos = ARObjectTransform.position;
        // pos.z += (isPositive ? 1f : -1f) * movingForce;

        // ARObjectTransform.position = pos;
    }
    void Move(Vector3 input){
        Vector3 camPos = Camera.main.transform.position;
        Vector3 a = camPos+input;
        Vector3 dirTemp = Quaternion.Euler(new Vector3(0,Camera.main.transform.rotation.eulerAngles.y,0))*(a-camPos).normalized;
        float speed = .5f;
        ARObjectTransform.position += new Vector3(dirTemp.x*speed,input.y*speed,dirTemp.z*speed);
    }
}
