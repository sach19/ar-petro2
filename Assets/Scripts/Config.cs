using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Config : MonoBehaviour
{
    [SerializeField] private Transform pathLayout;

    private SelectPath selectPath;

    private void Start()
    {
        selectPath = FindObjectOfType<SelectPath>();
        ResetPath();
    }

    public void SendPath()
    {
        for (int i = 0; i < pathLayout.childCount; i++)
        {
            selectPath.path[i] = pathLayout.GetChild(i).GetComponent<TMP_InputField>().text;
            PlayerPrefs.SetString($"path {i}", selectPath.path[i]);
        }
    }

    public void ResetPath()
    {
        StartCoroutine(ResetPathCor());
    }

    private IEnumerator ResetPathCor()
    {
        for (int i = 0; i < pathLayout.childCount; i++)
        {
            var field = pathLayout.GetChild(i).GetComponent<TMP_InputField>();
            field.Select();
            field.ActivateInputField();
            yield return null;
            field.MoveToStartOfLine(false, true);

            if (PlayerPrefs.HasKey($"path {i}"))
                field.text = PlayerPrefs.GetString($"path {i}");
            else
                field.text = "";
        }
    }
}
