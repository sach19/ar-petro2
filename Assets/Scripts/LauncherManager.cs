using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ButtonPath
{
    public Button button;
    public string path;
}

public class LauncherManager : MonoBehaviour
{
    public ButtonPath[] path;
    private void Awake()
    {
        foreach (var item in path)
        {
            item.button.onClick.AddListener(() => OpenApp(item.path));
        }
    }

    public void OpenApp(string path)
    {
        if (path != "") System.Diagnostics.Process.Start(path);
    }
}
