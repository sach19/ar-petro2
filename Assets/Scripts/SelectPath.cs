using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectPath : MonoBehaviour
{
    private ScenarioSelected scenarioSelected;
    [SerializeField] private CanvasGroupHandler videoUI;
    public string[] path;

    private void Start()
    {
        scenarioSelected = FindObjectOfType<ScenarioSelected>();
        for (int i = 0; i < 3; i++)
        {
            if (PlayerPrefs.HasKey($"path {i}"))
                path[i] = PlayerPrefs.GetString($"path {i}");
        }
    }

    public void OpenApp(string customPath = "")
    {
        if (scenarioSelected.currentSelected == 3)
        {
            videoUI.SetCanvas(true);
            return;
        }

        var target = customPath != "" ? customPath : path[scenarioSelected.currentSelected];

        if (target == "") return;
        System.Diagnostics.Process.Start(target);
    }
}
