using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LanguageHandler : MonoBehaviour
{
    private TextMeshProUGUI text;
    [SerializeField] private string inText;
    [SerializeField] private string enText;

    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    public void SetLanguage(bool en)
    {
        text.SetText(en ? enText : inText);
    }

}
