using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Video;
using DG.Tweening;
using System;

public class VideoScreen : MonoBehaviour
{
    public float initialScale;

    public Vector2 fullscreenSize;
    public Vector2 normalSize;

    private bool isOpen;
    private bool isFullscreen;
    private RectTransform rt;
    private VideoPlayer videoPlayer;
    private VideoControl videoControl;

    [SerializeField] private Button closeButton;
    [SerializeField] private Button fullscreenButton;
    [SerializeField] private Sprite[] fullscreenIcons;
    [SerializeField] private RawImage videoImage;
    [SerializeField] private Button overlayButton;
    [SerializeField] private UnityEvent OnVideoScreenOpen;

    private void OnDisable()
    {
        videoPlayer.targetTexture.Release();
    }

    private void Start()
    {
        rt = GetComponent<RectTransform>();
        videoPlayer = videoImage.GetComponent<VideoPlayer>();
        videoPlayer.targetTexture.Release();
        videoControl = GetComponentInChildren<VideoControl>();
        videoPlayer.loopPointReached += OnEndVideo;

        transform.localScale = Vector2.one * initialScale;
        isOpen = false;
        isFullscreen = false;

        /*var overlayColor = overlayButton.GetComponent<Image>().color;
        overlayColor.a = 0;
        overlayButton.GetComponent<Image>().color = overlayColor;
        overlayButton.gameObject.SetActive(false);*/

        closeButton.onClick.AddListener(CloseScreen);
        overlayButton.onClick.AddListener(CloseScreen);
        fullscreenButton.onClick.AddListener(SetFullscreen);

        OpenScreen(videoPlayer.clip);
    }

    private void OnEndVideo(VideoPlayer source)
    {
        StartCoroutine(OnEndVideoCor());
    }

    private IEnumerator OnEndVideoCor()
    {
        yield return new WaitForSeconds(.5f);
        CloseScreen();
    }

    public void OpenScreen(VideoClip clip)
    {
        if (isOpen) return;
        transform.DOScale(1, .25f);
        //SetOverlay(0, true);
        //overlayButton.GetComponent<Image>().DOFade(180f / 255f, .25f);
        isOpen = true;

        SetVideo(clip);
        videoControl.Init();

        if (OnVideoScreenOpen != null) OnVideoScreenOpen.Invoke();
    }

    private void SetVideo(VideoClip clip)
    {
        videoPlayer.clip = clip;
        videoPlayer.Play();
    }

    private void SetFullscreen()
    {
        isFullscreen = !isFullscreen;
        fullscreenButton.image.sprite = fullscreenIcons[isFullscreen ? 0 : 1];
        rt.DOSizeDelta(isFullscreen ? fullscreenSize : normalSize, .25f);
    }

    private void CloseScreen()
    {
        /*if (!isOpen) return;
        transform.DOScale(0, .25f);
        overlayButton.GetComponent<Image>().DOFade(0, .25f);
        SetOverlay(.25f, false);*/

        //videoPlayer.clip = null;
        videoPlayer.Stop();

        /*isOpen = false;*/
        isFullscreen = false;
        fullscreenButton.image.sprite = fullscreenIcons[isFullscreen ? 0 : 1];
        rt.DOSizeDelta(normalSize, .25f);

        VideoButtonTrigger.Instance.isVideoPlaying = true;
        VideoButtonTrigger.Instance.playBtn.image.sprite = VideoButtonTrigger.Instance.playIcons[VideoButtonTrigger.Instance.isVideoPlaying ? 0 : 1];
    }

    private void SetOverlay(float delay, bool value)
    {
        StartCoroutine(SetOverlayCor(delay, value));
    }

    private IEnumerator SetOverlayCor(float delay, bool value)
    {
        yield return new WaitForSeconds(delay);
        overlayButton.gameObject.SetActive(value);
    }
}
