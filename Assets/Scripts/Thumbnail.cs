using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Thumbnail : MonoBehaviour
{
    private VideoPlayer videoPlayer;
    private RawImage image;
    public int videoFrame;
    private void Awake()
    {
        image = GetComponent<RawImage>();
        videoPlayer = GetComponent<VideoPlayer>();
        videoPlayer.prepareCompleted += OnFrameReady;
        videoPlayer.sendFrameReadyEvents = true;
        videoPlayer.frame = videoFrame;
        videoPlayer.Play();
    }

    private void OnFrameReady(VideoPlayer video)
    {
        video.Pause();
        videoPlayer.sendFrameReadyEvents = false;
        Debug.Log($"{video.clip.name}");
    }
}
