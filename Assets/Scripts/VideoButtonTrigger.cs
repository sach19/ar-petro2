using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoButtonTrigger : MonoBehaviour
{
    public static VideoButtonTrigger Instance
    {
        get
        {
            return FindObjectOfType<VideoButtonTrigger>(true);
        }
    }

    private bool isButtonShowing;
    public bool isVideoPlaying;
    public Button playBtn;
    public Sprite[] playIcons;

    [SerializeField] private VideoPlayer videoPlayer;
    [SerializeField] private Button triggerBtn;
    [SerializeField] private GameObject buttonsGroup;

    long lastFrame;

    private void Start()
    {
        isVideoPlaying = false;
        isButtonShowing = true;
        playBtn.image.sprite = playIcons[1];

        playBtn.onClick.AddListener(PlayToggle);
        triggerBtn.onClick.AddListener(OnTrigger);
    }

    public void SetButtonStatus(bool value)
    {
        isButtonShowing = value;
    }

    private void OnEnable()
    {
        OnTrigger();
    }

    private void OnDisable()
    {
        isVideoPlaying = false;
        PlayToggle();
    }

    private void OnTrigger()
    {
        isButtonShowing = !isButtonShowing;
        ShowButtons(isButtonShowing);
    }

    private void ShowButtons(bool isButtonShowing)
    {
        if (!isButtonShowing)
        {
            buttonsGroup.SetActive(false);
        }
        else
        {
            buttonsGroup.SetActive(true);
        }
    }

    private void PlayToggle()
    {
        isVideoPlaying = !isVideoPlaying;
        playBtn.image.sprite = playIcons[isVideoPlaying ? 0 : 1];
        if (isVideoPlaying) videoPlayer.Pause();
        else videoPlayer.Play();

    }
}
