using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using TMPro;
using System;

public class VideoControl : MonoBehaviour
{
    [SerializeField] private VideoPlayer videoPlayer;
    [SerializeField] private TextMeshProUGUI currentTimeTMP;
    [SerializeField] private TextMeshProUGUI totalTimeTMP;
    [SerializeField] public Slider progressBar;

    private double currentTime, totalTime, countdownTimer;
    public bool isHandling;
    public void SetIsHandling(bool isHandling)
    {
        this.isHandling = isHandling;
    }

    public void SetClipTime()
    {
        var value = progressBar.value;
        videoPlayer.time = value * videoPlayer.clip.length;
    }

    private void Start()
    {
        videoPlayer.sendFrameReadyEvents = true;
        videoPlayer.frameReady += SetCurrentTime;
    }

    private void SetCurrentTime(VideoPlayer source, long frameIdx)
    {
        currentTime = source.time;
        countdownTimer = totalTime - currentTime;
        currentTimeTMP.text = ParseTime(countdownTimer);
        UpdateProgressBar();
    }

    public void Init()
    {
        totalTime = videoPlayer.clip.length;
        currentTimeTMP.text = ParseTime(totalTime);
        totalTimeTMP.text = ParseTime(totalTime);
    }

    private string ParseTime(double clipLength)
    {
        var formattedTime = System.TimeSpan.FromSeconds(clipLength);
        var minutes = formattedTime.Minutes.ToString();
        var seconds = formattedTime.Seconds.ToString();
        seconds = seconds.Length > 1 ? seconds : "0" + seconds;
        return $"{minutes}.{seconds}";
    }

    private void UpdateProgressBar()
    {
        if (isHandling) return;
        progressBar.value = (float)(currentTime / totalTime);
    }
}
